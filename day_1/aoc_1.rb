#!/usr/bin/env ruby

def input
  @input ||= STDIN.read.split("\n").map(&:to_i)
end

def calibrate_device
  input.reduce(0, :+)
end

puts calibrate_device if $PROGRAM_NAME == __FILE__
