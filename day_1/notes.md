<!-- require 'benchmark' -->

def calibrate_device_1
  input.inject(0) { |memo, item| memo + item }
end

def calibrate_device_2
  frecuency = 0
  input.map { |item| frecuency += item }
  frecuency
end

Benchmark.bm do |bm|
  bm.report { calibrate_device }
  bm.report { calibrate_device_1 }
  bm.report { calibrate_device_2 }
end

  user       system     total     real
  0.000013   0.000002   0.000015 (  0.000011)
  0.000115   0.000008   0.000123 (  0.000132)
  0.000088   0.000001   0.000089 (  0.000089)
