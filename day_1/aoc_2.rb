#!/usr/bin/env ruby

def input
  @input ||= STDIN.read.split("\n").map(&:to_i)
end

def calibrate_device
  frequency = 0

  input.cycle.inject([]) do |list, element|
    frequency += element
    return frequency if list.include?(frequency)

    list << frequency
  end
end

puts calibrate_device if $PROGRAM_NAME == __FILE__
